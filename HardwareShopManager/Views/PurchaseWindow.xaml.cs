﻿using HardwareShopManager.Commons;
using HardwareShopManager.Models;
using HardwareShopManager.ViewModels;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HardwareShopManager.Views
{
    /// <summary>
    /// Interaction logic for PurchaseWindow.xaml
    /// </summary>
    public partial class PurchaseWindow : MetroWindow
    {
        Purchase m_purchase;
        ObservableCollection<ProductTransactionViewModel> m_purchasedProducts;
        SQLiteConnection m_dbConn;
        bool m_isNew;

        public DialogResult Result { get; private set; }


        public PurchaseWindow(Purchase purchase, ObservableCollection<ProductViewModel> products, SQLiteConnection dbConn)
        {
            InitializeComponent();
            m_purchase = purchase;
            m_dbConn = dbConn;
            m_isNew = m_purchase == null;
            m_purchasedProducts = new ObservableCollection<ProductTransactionViewModel>();

            cmbProductName_AddProduct.ItemsSource = products; // List of all available products.
            lstPurchasedProducts.ItemsSource = m_purchasedProducts; // List of purchased products only.

            if (!m_isNew)
            {
                tboxID.Text = purchase.ID;
                tboxID.IsEnabled = false;
                dtpickPurchaseDate.SelectedDate = purchase.PurchaseDate;

                for (int i = 0; i < purchase.PurchasedProducts.Count; ++i)
                {
                    ProductTransactionViewModel vm = new ProductTransactionViewModel(purchase.PurchasedProducts[i]);
                    m_purchasedProducts.Add(vm);
                }
            }
            else
            {
                dtpickPurchaseDate.SelectedDate = DateTime.Now;
            }
        }

        private void btnAddProduct_Click(object sender, RoutedEventArgs e)
        {
            gridAddProduct.Visibility = Visibility.Visible;
        }

        private void btnRemoveProduct_Click(object sender, RoutedEventArgs e)
        {
            ProductTransactionViewModel selitem;
            if ((selitem = lstPurchasedProducts.SelectedItem as ProductTransactionViewModel) != null)
            {
                m_purchasedProducts.Remove(selitem);
            }
        }

        private void btnOK_AddProduct_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProductTransaction purchasedProduct = new ProductTransaction();
                purchasedProduct.Product = (cmbProductName_AddProduct.SelectedItem as ProductViewModel).Model;
                purchasedProduct.ProductID = purchasedProduct.Product.ID;
                purchasedProduct.Quantity = double.Parse(tboxQuantity_AddProduct.Text, CultureInfo.InvariantCulture);

                ProductTransactionViewModel vm = new ProductTransactionViewModel(purchasedProduct);
                if (m_purchasedProducts.Contains(vm))
                {
                    this.ShowMessageAsync("Error", "Product already exists!\nTo edit, please remove then add it again.");
                }
                else
                {
                    m_purchasedProducts.Add(vm);
                }

                btnCancel_AddProduct_Click(null, null); // Hiding the panel and resetting the values.
            }
            catch (Exception ex)
            {
                this.ShowMessageAsync("Error", "Unable to add product to the list.\nDetails: " + ex.Message);
            }
        }

        private void btnCancel_AddProduct_Click(object sender, RoutedEventArgs e)
        {
            cmbProductName_AddProduct.SelectedItem = null;
            tboxQuantity_AddProduct.Text = string.Empty;
            gridAddProduct.Visibility = Visibility.Hidden;
        }

        private async void btnOK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Checking the user inputs.
                string purchaseId = tboxID.Text;
                DateTime purchaseDate = dtpickPurchaseDate.SelectedDate.Value;

                if (string.IsNullOrEmpty(purchaseId))
                {
                    throw new Exception("purchaseId is null");
                }

                if (m_purchasedProducts.Count == 0)
                {
                    throw new Exception("No purchased product");
                }

                // Applying basic purchase info.
                using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                using (SQLiteTransaction transaction = m_dbConn.BeginTransaction())
                {
                    try
                    {
                        if (m_isNew)
                        {
                            cmd.CommandText = "INSERT INTO purchases(id, purchase_date) VALUES(@id, @purchaseDate)";
                        }
                        else
                        {
                            cmd.CommandText = "UPDATE purchases SET purchase_date = @purchaseDate WHERE id = @id";
                        }

                        cmd.Parameters.AddWithValue("id", purchaseId);
                        cmd.Parameters.AddWithValue("purchaseDate", purchaseDate);

                        await cmd.ExecuteNonQueryAsync();
                        cmd.Reset();

                        if (!m_isNew) // It's updating.
                        {
                            // Deleting purchase related data from purchase_product_link table.
                            cmd.CommandText = "DELETE FROM purchase_product_link WHERE purchase_id = @purchaseId";
                            cmd.Parameters.AddWithValue("purchaseId", purchaseId);
                            await cmd.ExecuteNonQueryAsync();
                            cmd.Reset();

                            // Subtracting products quantity.
                            for (int i = 0; i < m_purchase.PurchasedProducts.Count; ++i)
                            {
                                cmd.CommandText = "UPDATE products SET qty = qty - @purchasedQty WHERE id = @productId";
                                cmd.Parameters.AddWithValue("productId", m_purchase.PurchasedProducts[i].ProductID);
                                cmd.Parameters.AddWithValue("purchasedQty", m_purchase.PurchasedProducts[i].Quantity);
                                await cmd.ExecuteNonQueryAsync();
                                cmd.Reset();
                            }
                        }

                        // Adding purchased products to the link table.
                        for (int i = 0; i < m_purchasedProducts.Count; ++i)
                        {
                            ProductTransaction p = m_purchasedProducts[i].Model;

                            cmd.CommandText = "INSERT INTO purchase_product_link(purchase_id, product_id, qty) VALUES(@purchaseId, @productId, @purchasedQty)";
                            cmd.Parameters.AddWithValue("purchaseId", purchaseId);
                            cmd.Parameters.AddWithValue("productId", p.ProductID);
                            cmd.Parameters.AddWithValue("purchasedQty", p.Quantity);
                            await cmd.ExecuteNonQueryAsync();
                            cmd.Reset();

                            // Adding the product's stock.
                            cmd.CommandText = "UPDATE products SET qty = qty + @purchasedQuantity WHERE id = @productId";
                            cmd.Parameters.AddWithValue("productId", p.ProductID);
                            cmd.Parameters.AddWithValue("purchasedQuantity", p.Quantity);
                            await cmd.ExecuteNonQueryAsync();
                            cmd.Reset();
                        }

                        // All done!
                        transaction.Commit();
                        Result = Commons.DialogResult.OK;
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                await this.ShowMessageAsync("Error", "Unable to apply changes.\nDetails: " + ex.Message);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
