﻿using HardwareShopManager.Commons;
using HardwareShopManager.Models;
using HardwareShopManager.ViewModels;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HardwareShopManager.Views
{
    /// <summary>
    /// Interaction logic for BillWindow.xaml
    /// </summary>
    public partial class BillWindow : MetroWindow
    {
        static List<string> m_billType;
        ObservableCollection<ProductTransactionViewModel> m_soldProducts;
        SQLiteConnection m_dbConn;
        Bill m_bill;
        bool m_isNew;

        public DialogResult Result { get; private set; }


        static BillWindow()
        {
            m_billType = new List<string>();
            m_billType.Add("CASH");
            m_billType.Add("CREDIT");
        }

        public BillWindow(Bill bill, ObservableCollection<ProductViewModel> products, SQLiteConnection conn)
        {
            InitializeComponent();
            m_dbConn = conn;
            m_bill = bill;
            m_isNew = m_bill == null;
            m_soldProducts = new ObservableCollection<ProductTransactionViewModel>();

            cmbBillType.ItemsSource = m_billType;
            cmbProduct_AddProduct.ItemsSource = products;
            lstSoldProducts.ItemsSource = m_soldProducts;

            if (!m_isNew)
            {
                tboxID.Text = bill.ID;
                tboxID.IsEnabled = false;
                tboxCustName.Text = bill.CustomerName;
                tboxCustAddress.Text = bill.CustomerAddress;
                tboxCustPhone.Text = bill.CustomerPhone;
                dtpickBillDateTime.SelectedDate = bill.BillDate;

                if (bill.Type == Bill.BillType.Cash)
                {
                    cmbBillType.SelectedItem = "CASH";
                }
                else
                {
                    cmbBillType.SelectedItem = "CREDIT";
                }

                for (int i = 0; i < bill.SoldProducts.Count; ++i)
                {
                    ProductTransactionViewModel ptvm = new ProductTransactionViewModel(bill.SoldProducts[i]);
                    m_soldProducts.Add(ptvm);
                }
            }
            else
            {
                dtpickBillDateTime.SelectedDate = DateTime.Now;
            }
        }

        private void btnAddProduct_Click(object sender, RoutedEventArgs e)
        {
            gridAddProduct.Visibility = Visibility.Visible;
        }

        private void btnRemoveProduct_Click(object sender, RoutedEventArgs e)
        {
            ProductTransactionViewModel selitem;
            if ((selitem = lstSoldProducts.SelectedItem as ProductTransactionViewModel) != null)
            {
                m_soldProducts.Remove(selitem);
            }
        }

        private void btnOK_AddProduct_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProductTransaction pt = new ProductTransaction();
                pt.Product = (cmbProduct_AddProduct.SelectedItem as ProductViewModel).Model;
                pt.ProductID = pt.Product.ID;
                pt.Quantity = double.Parse(tboxQuantity_AddProduct.Text, CultureInfo.InvariantCulture);

                ProductTransactionViewModel ptvm = new ProductTransactionViewModel(pt);
                if (m_soldProducts.Contains(ptvm))
                {
                    this.ShowMessageAsync("Error", "Product already exists!\nTo edit, please remove then add it again.");
                }
                else
                {
                    m_soldProducts.Add(ptvm);
                }

                btnCancel_AddProduct_Click(null, null);
            }
            catch (Exception ex)
            {
                this.ShowMessageAsync("Error", "Unable to add product to the list.\nDetails: " + ex.Message);
            }
        }

        private void btnCancel_AddProduct_Click(object sender, RoutedEventArgs e)
        {
            cmbProduct_AddProduct.SelectedItem = null;
            tboxQuantity_AddProduct.Text = string.Empty;
            gridAddProduct.Visibility = Visibility.Hidden;
        }

        private async void btnOK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Checking the user inputs.
                string billId = tboxID.Text;
                string billType = cmbBillType.SelectedItem as string;
                string custName = tboxCustName.Text;
                string custPhone = tboxCustPhone.Text;
                string custAddress = tboxCustAddress.Text;
                DateTime billTime = dtpickBillDateTime.SelectedDate.Value;

                if (string.IsNullOrEmpty(billId))
                {
                    throw new Exception("billId is null");
                }

                if (string.IsNullOrEmpty(billType))
                {
                    throw new Exception("billType is null");
                }

                if (string.IsNullOrEmpty(custName))
                {
                    throw new Exception("custName is null");
                }

                if (m_soldProducts.Count == 0)
                {
                    throw new Exception("No sold product");
                }

                using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                using (SQLiteTransaction transaction = m_dbConn.BeginTransaction())
                {
                    try
                    {
                        // Updating the bill's info.
                        if (m_isNew)
                        {
                            cmd.CommandText = "INSERT INTO bills(id, type, customer_name, customer_phone, customer_address, bill_date) VALUES(@id, @billType, @custName, @custPhone, @custAddress, @billDate)";
                        }
                        else
                        {
                            cmd.CommandText = "UPDATE bills SET type = @billType, customer_name = @custName, customer_phone = @custPhone, customer_address = @custAddress, bill_date = @billDate WHERE id = @id";
                        }

                        cmd.Parameters.AddWithValue("id", billId);
                        cmd.Parameters.AddWithValue("billType", billType);
                        cmd.Parameters.AddWithValue("custName", custName);
                        cmd.Parameters.AddWithValue("custAddress", custAddress);
                        cmd.Parameters.AddWithValue("custPhone", custPhone);
                        cmd.Parameters.AddWithValue("billDate", billTime);

                        await cmd.ExecuteNonQueryAsync();
                        cmd.Reset();

                        if (!m_isNew)
                        {
                            // Clearing the link table.
                            cmd.CommandText = "DELETE FROM bill_product_link WHERE bill_id = @billId";
                            cmd.Parameters.AddWithValue("billId", billId);
                            await cmd.ExecuteNonQueryAsync();
                            cmd.Reset();

                            // Adding the stock.
                            for (int i = 0; i < m_bill.SoldProducts.Count; ++i)
                            {
                                double soldQty = m_bill.SoldProducts[i].Quantity;
                                string productId = m_bill.SoldProducts[i].ProductID;
                                cmd.CommandText = "UPDATE products SET qty = qty + @soldQty WHERE id = @productId";
                                cmd.Parameters.AddWithValue("productId", productId);
                                cmd.Parameters.AddWithValue("qty", soldQty);
                                await cmd.ExecuteNonQueryAsync();
                                cmd.Reset();
                            }
                        }

                        // Adding all sold products to the link table.
                        for (int i = 0; i < m_soldProducts.Count; ++i)
                        {
                            ProductTransaction p = m_soldProducts[i].Model;

                            if (p.Quantity > p.Product.Quantity)
                            {
                                // Sold quantity cant be more than the product's stock.
                                throw new Exception("Sold quantity is greater than product's stock");
                            }

                            cmd.CommandText = "INSERT INTO bill_product_link(bill_id, product_id, qty) VALUES(@billId, @productId, @soldQty)";
                            cmd.Parameters.AddWithValue("billId", billId);
                            cmd.Parameters.AddWithValue("productId", p.ProductID);
                            cmd.Parameters.AddWithValue("soldQty", p.Quantity);
                            await cmd.ExecuteNonQueryAsync();
                            cmd.Reset();

                            // Subtracting the product's stock.
                            cmd.CommandText = "UPDATE products SET qty = qty - @soldQty WHERE id = @productId";
                            cmd.Parameters.AddWithValue("productId", p.ProductID);
                            cmd.Parameters.AddWithValue("soldQty", p.Quantity);
                            await cmd.ExecuteNonQueryAsync();
                            cmd.Reset();
                        }

                        // All done!.
                        transaction.Commit();
                        Result = Commons.DialogResult.OK;
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                await this.ShowMessageAsync("Error", "Unable to apply changes.\nDetails: " + ex.Message);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
