﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HardwareShopManager.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            Init();
        }

        void Init()
        {
            // Redirecting to the login page.
            LoginPage loginPage = new LoginPage(this);
            frmMain.Navigate(loginPage);
        }

        public void Navigate(Page page)
        {
            frmMain.Navigate(page);
        }

        public async void ShowMessage(string title, string message)
        {
            await this.ShowMessageAsync(title, message);
        }
    }
}
