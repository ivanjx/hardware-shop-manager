﻿using HardwareShopManager.Commons;
using HardwareShopManager.Models;
using HardwareShopManager.ViewModels;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HardwareShopManager.Views
{
    /// <summary>
    /// Interaction logic for VendorWindow.xaml
    /// </summary>
    public partial class VendorWindow : MetroWindow
    {
        SQLiteConnection m_dbConn;
        bool m_isNew;

        public DialogResult Result { get; private set; }


        public VendorWindow(Vendor vendor, SQLiteConnection conn)
        {
            InitializeComponent();
            m_dbConn = conn;
            Result = Commons.DialogResult.None;

            if (vendor == null)
            {
                m_isNew = true;
            }
            else
            {
                tboxID.IsEnabled = false;
                tboxID.Text = vendor.ID;
                tboxName.Text = vendor.Name;
                tboxPhone.Text = vendor.Phone;
                tboxAddress.Text = vendor.Address;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Result = Commons.DialogResult.Cancel;
            this.Close();
        }

        private async void btnOK_Click(object sender, RoutedEventArgs e)
        {
            // Freezing elements.
            gridForm.IsEnabled = false;
            btnOK.IsEnabled = false;
            btnCancel.IsEnabled = false;

            // Applying changes to the db.
            string id = tboxID.Text;
            string name = tboxName.Text;
            string phone = tboxPhone.Text;
            string address = tboxAddress.Text;

            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    throw new Exception("ID is null");
                }

                if (string.IsNullOrEmpty(name))
                {
                    throw new Exception("Name is null");
                }

                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = m_dbConn;

                    if (m_isNew)
                    {
                        cmd.CommandText = "INSERT INTO vendors(id, name, phone, address) VALUES(@id,@name,@phone,@address)";
                    }
                    else
                    {
                        cmd.CommandText = "UPDATE vendors SET name = @name, phone = @phone, address = @address WHERE id = @id";
                    }

                    cmd.Parameters.AddWithValue("id", id);
                    cmd.Parameters.AddWithValue("name", name);
                    cmd.Parameters.AddWithValue("phone", phone);
                    cmd.Parameters.AddWithValue("address", address);

                    int changedRowsCount = await cmd.ExecuteNonQueryAsync();

                    if (changedRowsCount != 1)
                    {
                        throw new Exception("Invalid row changes count");
                    }
                }

                Result = Commons.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                await this.ShowMessageAsync("Error", "Unable to apply changes.\nDetails: " + ex.Message);
            }
            finally
            {
                gridForm.IsEnabled = true;
                btnOK.IsEnabled = true;
                btnCancel.IsEnabled = true;
            }
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            
        }
    }
}
