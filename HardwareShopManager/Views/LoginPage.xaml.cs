﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HardwareShopManager.Views
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        MainWindow m_mainwindow;
        const string DB_FILE_NAME = "DB";
        bool dbFileChecked = false;


        public LoginPage(MainWindow mainwindow)
        {
            InitializeComponent();
            m_mainwindow = mainwindow;
            CheckDBFile();
        }

        void ChangeState(bool isLoading)
        {
            if (isLoading)
            {
                pwboxPassword.IsEnabled = false;
                btnLogin.IsEnabled = false;
                pringLoading.IsActive = true;
            }
            else
            {
                pwboxPassword.IsEnabled = true;
                btnLogin.IsEnabled = true;
                pringLoading.IsActive = false;
            }
        }

        async void CheckDBFile()
        {
            async Task CreateTables(SQLiteConnection conn)
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;

                    using (StreamReader reader = new StreamReader(File.Open("Resources/Scripts/init.sql", FileMode.Open)))
                    {
                        string script = await reader.ReadToEndAsync();
                        cmd.CommandText = script;
                        await cmd.ExecuteNonQueryAsync();
                    }
                }
            }

            try
            {
                if (File.Exists(DB_FILE_NAME))
                {
                    return;
                }

                SQLiteConnection.CreateFile(DB_FILE_NAME);
                using (SQLiteConnection conn = new SQLiteConnection(string.Format("Data source={0};", DB_FILE_NAME)))
                {
                    await conn.OpenAsync();
                    await CreateTables(conn);
                }
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", ex.Message);
            }
            finally
            {
                dbFileChecked = true;
            }
        }

        private async void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            // Checking the user inputs.
            string username = tboxUsername.Text;
            string password = pwboxPassword.Password;

            if (string.IsNullOrEmpty(password))
            {
                m_mainwindow.ShowMessage("Error", "Please specify the password!");
                return;
            }

            ChangeState(true);

            while (!dbFileChecked)
            {
                await Task.Delay(100);
            }

            // All ok!
            try
            {
                // Opening connection.
                SQLiteConnection conn = new SQLiteConnection(string.Format("Data source={0};", DB_FILE_NAME));
                await conn.OpenAsync();

                // Verifying login info.
                string sql = "SELECT * FROM users WHERE uname = @username AND password = @password";
                using (SQLiteCommand cmd = new SQLiteCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("username", username);
                    cmd.Parameters.AddWithValue("password", password);

                    object result = await cmd.ExecuteScalarAsync();
                    string uname = result as string;

                    if (!string.IsNullOrEmpty(username) && uname == username)
                    {
                        // Navigating to home page.
                        HomePage homepage = new HomePage(m_mainwindow, conn);
                        m_mainwindow.Navigate(homepage);
                    }
                    else
                    {
                        throw new Exception("Unable to login");
                    }
                }
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", ex.Message);
            }
            finally
            {
                ChangeState(false);
                tboxUsername.Clear();
                pwboxPassword.Clear();
            }
        }
    }
}
