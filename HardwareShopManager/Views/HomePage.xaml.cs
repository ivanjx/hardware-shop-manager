﻿using HardwareShopManager.Commons;
using HardwareShopManager.Models;
using HardwareShopManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HardwareShopManager.Views
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class HomePage : Page
    {
        MainWindow m_mainwindow;
        SQLiteConnection m_dbConn;
        ObservableCollection<VendorViewModel> m_vendors;
        ObservableCollection<ProductViewModel> m_products;
        ObservableCollection<PurchaseViewModel> m_purchases;
        ObservableCollection<BillViewModel> m_bills;
        bool m_isLoadingVendors;
        bool m_isLoadingProducts;
        bool m_isLoadingPurchases;
        bool m_isLoadingBills;


        public HomePage(MainWindow mainwindow, SQLiteConnection conn)
        {
            InitializeComponent();
            m_mainwindow = mainwindow;
            m_dbConn = conn;

            m_vendors = new ObservableCollection<VendorViewModel>();
            m_vendors.CollectionChanged += (sender, e) =>
            {
                if (m_vendors.Count > 0)
                {
                    lblLstVendorsInfo.Visibility = Visibility.Hidden;
                }
                else
                {
                    lblLstVendorsInfo.Visibility = Visibility.Visible;
                }
            };
            lstVendors.ItemsSource = m_vendors;
            lstVendors.SelectionChanged += (sender, e) =>
            {
                if (lstVendors.SelectedItem != null)
                {
                    btnRemoveVendor.Visibility = Visibility.Visible;
                    btnEditVendor.Visibility = Visibility.Visible;
                }
                else
                {
                    btnRemoveVendor.Visibility = Visibility.Hidden;
                    btnEditVendor.Visibility = Visibility.Hidden;
                }
            };

            m_products = new ObservableCollection<ProductViewModel>();
            m_products.CollectionChanged += (sender, e) =>
            {
                if (m_products.Count > 0)
                {
                    lblLstProductsInfo.Visibility = Visibility.Hidden;
                }
                else
                {
                    lblLstProductsInfo.Visibility = Visibility.Visible;
                }
            };
            lstProducts.ItemsSource = m_products;
            lstProducts.SelectionChanged += (sender, e) =>
            {
                if (lstProducts.SelectedItem != null)
                {
                    btnRemoveProduct.Visibility = Visibility.Visible;
                    btnEditProduct.Visibility = Visibility.Visible;
                }
                else
                {
                    btnRemoveProduct.Visibility = Visibility.Hidden;
                    btnEditProduct.Visibility = Visibility.Hidden;
                }
            };

            m_purchases = new ObservableCollection<PurchaseViewModel>();
            m_purchases.CollectionChanged += (sender, e) =>
            {
                if (m_purchases.Count > 0)
                {
                    lblLstPurchasesInfo.Visibility = Visibility.Hidden;
                }
                else
                {
                    lblLstPurchasesInfo.Visibility = Visibility.Visible;
                }
            };
            lstPurchases.ItemsSource = m_purchases;
            lstPurchases.SelectionChanged += (sender, e) =>
            {
                if (lstPurchases.SelectedItem != null)
                {
                    btnRemovePurchase.Visibility = Visibility.Visible;
                    btnEditPurchase.Visibility = Visibility.Visible;
                }
                else
                {
                    btnRemovePurchase.Visibility = Visibility.Hidden;
                    btnEditPurchase.Visibility = Visibility.Hidden;
                }
            };

            m_bills = new ObservableCollection<BillViewModel>();
            m_bills.CollectionChanged += (sender, e) =>
            {
                if (m_bills.Count > 0)
                {
                    lblLstBillsInfo.Visibility = Visibility.Hidden;
                }
                else
                {
                    lblLstBillsInfo.Visibility = Visibility.Visible;
                }
            };
            lstBills.ItemsSource = m_bills;
            lstBills.SelectionChanged += (sender, e) =>
            {
                if (lstBills.SelectedItem != null)
                {
                    btnRemoveBill.Visibility = Visibility.Visible;
                    btnEditBill.Visibility = Visibility.Visible;
                }
                else
                {
                    btnRemoveBill.Visibility = Visibility.Hidden;
                    btnEditBill.Visibility = Visibility.Hidden;
                }
            };

            LoadData();
        }

        async void LoadData()
        {
            await LoadLstVendors();
            await LoadLstProducts();
            await LoadLstPurchases();
            await LoadLstBills();
        }

        async Task LoadLstVendors()
        {
            if (m_isLoadingVendors)
            {
                return;
            }

            m_isLoadingVendors = true;
            m_vendors.Clear();

            try
            {
                List<Vendor> vendors = await Data.LoadVendorsAsync(m_dbConn);

                for (int i = 0; i < vendors.Count; ++i)
                {
                    VendorViewModel vm = new VendorViewModel(vendors[i]);
                    m_vendors.Add(vm);
                    await Task.Delay(15);
                }
            }
            finally
            {
                m_isLoadingVendors = false;
            }
        }

        async Task LoadLstProducts()
        {
            if (m_isLoadingProducts)
            {
                return;
            }

            m_isLoadingProducts = true;
            m_products.Clear();

            try
            {
                List<Product> products = await Data.LoadProductsAsync(m_dbConn);

                for (int i = 0; i < products.Count; ++i)
                {
                    ProductViewModel vm = new ProductViewModel(products[i]);
                    m_products.Add(vm);
                    await Task.Delay(15);
                }
            }
            finally
            {
                m_isLoadingProducts = false;
            }
        }

        async Task LoadLstPurchases()
        {
            if (m_isLoadingPurchases)
            {
                return;
            }

            m_isLoadingPurchases = true;
            m_purchases.Clear();

            try
            {
                List<Purchase> purchases = await Data.LoadPurchasesAsync(m_dbConn);

                for (int i = 0; i < purchases.Count; ++i)
                {
                    PurchaseViewModel vm = new PurchaseViewModel(purchases[i]);
                    m_purchases.Add(vm);
                    await Task.Delay(15);
                }
            }
            finally
            {
                m_isLoadingPurchases = false;
            }
        }

        async Task LoadLstBills()
        {
            if (m_isLoadingBills)
            {
                return;
            }

            m_isLoadingBills = true;
            m_bills.Clear();

            try
            {
                List<Bill> bills = await Data.LoadBillsAsync(m_dbConn);

                for (int i = 0; i < bills.Count; ++i)
                {
                    BillViewModel bvm = new BillViewModel(bills[i]);
                    m_bills.Add(bvm);
                    await Task.Delay(15);
                }
            }
            finally
            {
                m_isLoadingBills = false;
            }
        }

        private async void btnRefreshVendors_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                await LoadLstVendors();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to refresh vendors.\nDetails: " + ex.Message);
            }
        }

        private async void btnAddVendor_Click(object sender, RoutedEventArgs e)
        {
            VendorWindow vendorWindow = new VendorWindow(null, m_dbConn);
            vendorWindow.ShowDialog();

            if (vendorWindow.Result == Commons.DialogResult.OK)
            {
                await LoadLstVendors();
            }
        }

        private async void btnRemoveVendor_Click(object sender, RoutedEventArgs e)
        {
            VendorViewModel selitem;
            if ((selitem = lstVendors.SelectedItem as VendorViewModel) != null)
            {
                try
                {
                    using (SQLiteCommand cmd = new SQLiteCommand())
                    {
                        cmd.Connection = m_dbConn;
                        cmd.CommandText = "DELETE FROM vendors WHERE id = @id";
                        cmd.Parameters.AddWithValue("id", selitem.Model.ID);
                        await cmd.ExecuteNonQueryAsync();
                    }

                    m_vendors.Remove(selitem);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete a vendor.\nDetails: " + ex.Message);
                }
            }
        }

        private async void btnEditVendor_Click(object sender, RoutedEventArgs e)
        {
            VendorViewModel selitem;
            if ((selitem = lstVendors.SelectedItem as VendorViewModel) != null)
            {
                VendorWindow vendorWindow = new VendorWindow(selitem.Model, m_dbConn);
                vendorWindow.ShowDialog();

                if (vendorWindow.Result == Commons.DialogResult.OK)
                {
                    await LoadLstVendors();
                }
            }
        }

        private async void btnRefreshProducts_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                await LoadLstProducts();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to refresh products.\nDetails: " + ex.Message);
            }
        }

        private async void btnAddProduct_Click(object sender, RoutedEventArgs e)
        {
            ProductWindow productWindow = new ProductWindow(null, m_vendors, m_dbConn);
            productWindow.ShowDialog();

            if (productWindow.Result == Commons.DialogResult.OK)
            {
                await LoadLstProducts();
            }
        }

        private async void btnEditProduct_Click(object sender, RoutedEventArgs e)
        {
            ProductViewModel selitem;
            if ((selitem = lstProducts.SelectedItem as ProductViewModel) != null)
            {
                ProductWindow productWindow = new ProductWindow(selitem.Model, m_vendors, m_dbConn);
                productWindow.ShowDialog();

                if (productWindow.Result == Commons.DialogResult.OK)
                {
                    await LoadLstProducts();
                }
            }
        }

        private async void btnRemoveProduct_Click(object sender, RoutedEventArgs e)
        {
            ProductViewModel selitem;
            if ((selitem = lstProducts.SelectedItem as ProductViewModel) != null)
            {
                try
                {
                    using (SQLiteCommand cmd = new SQLiteCommand())
                    {
                        cmd.Connection = m_dbConn;
                        cmd.CommandText = "DELETE FROM products WHERE id = @id";
                        cmd.Parameters.AddWithValue("id", selitem.Model.ID);
                        await cmd.ExecuteNonQueryAsync();
                    }

                    m_products.Remove(selitem);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete a product.\nDetails: " + ex.Message);
                }
            }
        }

        private async void btnRefreshPurchases_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                await LoadLstPurchases();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to refresh purchases list.\nDetails: " + ex.Message);
            }
        }

        private async void btnAddPurchase_Click(object sender, RoutedEventArgs e)
        {
            PurchaseWindow purchaseWindow = new PurchaseWindow(null, m_products, m_dbConn);
            purchaseWindow.ShowDialog();

            if (purchaseWindow.Result == Commons.DialogResult.OK)
            {
                await LoadLstProducts();
                await LoadLstPurchases();
            }
        }

        private async void btnRemovePurchase_Click(object sender, RoutedEventArgs e)
        {
            PurchaseViewModel selitem;
            if ((selitem = lstPurchases.SelectedItem as PurchaseViewModel) != null)
            {
                try
                {
                    using (SQLiteCommand cmd = new SQLiteCommand())
                    using (SQLiteTransaction transaction = m_dbConn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Connection = m_dbConn;
                            cmd.CommandText = "DELETE FROM purchases WHERE id = @id";
                            cmd.Parameters.AddWithValue("id", selitem.Model.ID);
                            await cmd.ExecuteNonQueryAsync();
                            cmd.Reset();

                            // Subtracting the product's stock.
                            for (int i = 0; i < selitem.Model.PurchasedProducts.Count; ++i)
                            {
                                cmd.CommandText = "UPDATE products SET qty = qty - @purchasedQty WHERE id = @productId";
                                cmd.Parameters.AddWithValue("productId", selitem.Model.PurchasedProducts[i].ProductID);
                                cmd.Parameters.AddWithValue("purchasedQty", selitem.Model.PurchasedProducts[i].Quantity);
                                await cmd.ExecuteNonQueryAsync();
                                cmd.Reset();
                            }

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }

                    m_purchases.Remove(selitem);
                    await LoadLstProducts();
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete a purchase.\nDetails: " + ex.Message);
                }
            }
        }

        private async void btnEditPurchase_Click(object sender, RoutedEventArgs e)
        {
            PurchaseViewModel selitem;
            if ((selitem = lstPurchases.SelectedItem as PurchaseViewModel) != null)
            {
                PurchaseWindow purchaseWindow = new PurchaseWindow(selitem.Model, m_products, m_dbConn);
                purchaseWindow.ShowDialog();

                if (purchaseWindow.Result == DialogResult.OK)
                {
                    await LoadLstPurchases();
                    await LoadLstProducts();
                }
            }
        }

        private async void btnRefreshBills_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                await LoadLstBills();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to refresh bills list.\nDetails: " + ex.Message);
            }
        }

        private async void btnAddBill_Click(object sender, RoutedEventArgs e)
        {
            BillWindow billWindow = new BillWindow(null, m_products, m_dbConn);
            billWindow.ShowDialog();

            if (billWindow.Result == DialogResult.OK)
            {
                await LoadLstBills();
                await LoadLstProducts();
            }
        }

        private async void btnRemoveBill_Click(object sender, RoutedEventArgs e)
        {
            BillViewModel selitem;
            if ((selitem = lstBills.SelectedItem as BillViewModel) != null)
            {
                try
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    using (SQLiteTransaction transaction = m_dbConn.BeginTransaction())
                    {
                        try
                        {
                            cmd.CommandText = "DELETE FROM bills WHERE id = @id";
                            cmd.Parameters.AddWithValue("id", selitem.ID);
                            await cmd.ExecuteNonQueryAsync();
                            cmd.Reset();

                            // Adding the products stock.
                            for (int i = 0; i < selitem.Model.SoldProducts.Count; ++i)
                            {
                                cmd.CommandText = "UPDATE products SET qty = qty + @soldQty WHERE id = @productId";
                                cmd.Parameters.AddWithValue("productId", selitem.Model.SoldProducts[i].ProductID);
                                cmd.Parameters.AddWithValue("soldQty", selitem.Model.SoldProducts[i].Quantity);
                                await cmd.ExecuteNonQueryAsync();
                                cmd.Reset();
                            }

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }

                    m_bills.Remove(selitem);
                    await LoadLstProducts();
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to remove a bill.\nDetails: " + ex.Message);
                }
            }
        }

        private async void btnEditBill_Click(object sender, RoutedEventArgs e)
        {
            BillViewModel selitem;
            if ((selitem = lstBills.SelectedItem as BillViewModel) != null)
            {
                BillWindow billWindow = new BillWindow(selitem.Model, m_products, m_dbConn);
                billWindow.ShowDialog();

                if (billWindow.Result == DialogResult.OK)
                {
                    await LoadLstBills();
                    await LoadLstProducts();
                }
            }
        }
    }
}
