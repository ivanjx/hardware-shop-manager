﻿using HardwareShopManager.Commons;
using HardwareShopManager.Models;
using HardwareShopManager.ViewModels;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HardwareShopManager.Views
{
    /// <summary>
    /// Interaction logic for ProductWindow.xaml
    /// </summary>
    public partial class ProductWindow : MetroWindow
    {
        SQLiteConnection m_dbConn;
        bool m_isNew;

        public DialogResult Result { get; private set; }


        public ProductWindow(Product product, ObservableCollection<VendorViewModel> vendors, SQLiteConnection conn)
        {
            InitializeComponent();
            Result = Commons.DialogResult.None;
            m_dbConn = conn;
            cmbVendor.ItemsSource = vendors;

            if (product == null) // New data.
            {
                m_isNew = true;
                tboxUnitName.Text = "Unit";
                tboxQuantity.Text = "0";
                tboxBuyPrice.Text = "0";
                tboxSellPrice.Text = "0";
                tboxVAT.Text = "0";
            }
            else // Edit data.
            {
                tboxID.Text = product.ID;
                tboxID.IsEnabled = false;
                tboxName.Text = product.Name;

                // Finding vendor.
                for (int i = 0; i < vendors.Count; ++i)
                {
                    if (product.VendorID == vendors[i].Model.ID)
                    {
                        cmbVendor.SelectedItem = vendors[i];
                    }
                }

                tboxUnitName.Text = product.UnitName;
                tboxQuantity.Text = product.Quantity.ToString();
                tboxBuyPrice.Text = product.BuyPrice.ToString();
                tboxSellPrice.Text = product.SellPrice.ToString();
                tboxVAT.Text = product.VAT.ToString();
            }
        }

        private async void btnOK_Click(object sender, RoutedEventArgs e)
        {
            // Freezing elements.
            gridForm.IsEnabled = false;
            btnOK.IsEnabled = false;
            btnCancel.IsEnabled = false;

            try
            {
                // Checking user inputs.
                string id = tboxID.Text;
                string name = tboxName.Text;
                VendorViewModel vendorVM = cmbVendor.SelectedItem as VendorViewModel;
                string unitName = tboxUnitName.Text;
                int quantity = int.Parse(tboxQuantity.Text);
                decimal buyPrice = decimal.Parse(tboxBuyPrice.Text, CultureInfo.InvariantCulture);
                decimal sellPrice = decimal.Parse(tboxSellPrice.Text, CultureInfo.InvariantCulture);
                float vat = float.Parse(tboxVAT.Text, CultureInfo.InvariantCulture);

                if (string.IsNullOrEmpty(id))
                {
                    throw new Exception("ID is null");
                }

                if (string.IsNullOrEmpty(name))
                {
                    throw new Exception("Name is null");
                }

                if (vendorVM == null)
                {
                    throw new Exception("Vendor not selected");
                }

                if (string.IsNullOrEmpty(unitName))
                {
                    throw new Exception("Unit name is null");
                }

                if (buyPrice > sellPrice)
                {
                    throw new Exception("buyPrice is greater than sellPrice");
                }

                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = m_dbConn;

                    if (m_isNew)
                    {
                        cmd.CommandText = "INSERT INTO products(id, name, vendor_id, qty, unit_name, price_buy, price_sell, vat) ";
                        cmd.CommandText += "VALUES(@id, @name, @vendorId, @quantity, @unitName, @buyPrice, @sellPrice, @vat)";
                    }
                    else
                    {
                        cmd.CommandText = "UPDATE products SET name = @name, vendor_id = @vendorId, qty = @quantity, unit_name = @unitName, price_buy = @buyPrice, price_sell = @sellPrice, vat = @vat WHERE id = @id";
                    }

                    cmd.Parameters.AddWithValue("id", id);
                    cmd.Parameters.AddWithValue("name", name);
                    cmd.Parameters.AddWithValue("vendorId", vendorVM.Model.ID);
                    cmd.Parameters.AddWithValue("quantity", quantity);
                    cmd.Parameters.AddWithValue("unitName", unitName);
                    cmd.Parameters.AddWithValue("buyPrice", buyPrice);
                    cmd.Parameters.AddWithValue("sellPrice", sellPrice);
                    cmd.Parameters.AddWithValue("vat", vat);

                    int result = await cmd.ExecuteNonQueryAsync();

                    if (result != 1)
                    {
                        throw new Exception("Data not changed properly");
                    }

                    // Done.
                    Result = Commons.DialogResult.OK;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                await this.ShowMessageAsync("Error", "Unable to apply changes.\nDetails: " + ex.Message);
            }
            finally
            {
                // Un-freezing elements.
                gridForm.IsEnabled = true;
                btnOK.IsEnabled = true;
                btnCancel.IsEnabled = true;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Result = Commons.DialogResult.Cancel;
            this.Close();
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            
        }
    }
}
