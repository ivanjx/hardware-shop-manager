-- users table.
CREATE TABLE users (
    uname VARCHAR(20) NOT NULL,
    password VARCHAR(30) NOT NULL,
    PRIMARY KEY(uname)
);

-- company_details table.
CREATE TABLE company_details (
    name VARCHAR(30) NOT NULL,
    address TEXT NOT NULL
);

-- vendors table.
CREATE TABLE vendors (
    id VARCHAR(30) NOT NULL,
    name VARCHAR(30) NOT NULL,
    phone VARCHAR(20),
    address TEXT,
    PRIMARY KEY(id)
);

-- products table.
CREATE TABLE products (
    id VARCHAR(30) NOT NULL,
    name VARCHAR(30) NOT NULL,
    vendor_id VARCHAR(30) NOT NULL,
    unit_name VARCHAR(10) NOT NULL,
    qty REAL NOT NULL,
    price_buy REAL NOT NULL,
    price_sell REAL NOT NULL,
    vat REAL NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(vendor_id) REFERENCES vendors(id) ON DELETE CASCADE
);

-- purchases table.
CREATE TABLE purchases (
    id VARCHAR(30) NOT NULL,
    purchase_date DATETIME NOT NULL,
    PRIMARY KEY(id)
);

-- purchase_product_link table.
CREATE TABLE purchase_product_link (
	purchase_id VARCHAR(30) NOT NULL,
	product_id VARCHAR(30) NOT NULL,
	qty REAL NOT NULL,
	FOREIGN KEY(purchase_id) REFERENCES purchases(id) ON DELETE CASCADE,
	FOREIGN KEY(product_id) REFERENCES products(id)
);

-- bills table.
CREATE table bills (
	id VARCHAR(30) NOT NULL,
	type VARCHAR(6) NOT NULL, -- CASH or CREDIT.
	customer_name VARCHAR(30) NOT NULL,
	customer_address TEXT,
	customer_phone VARCHAR(20),
	bill_date DATETIME NOT NULL,
	PRIMARY KEY(id)
);

-- bill_product_link table.
CREATE TABLE bill_product_link (
	bill_id VARCHAR(30) NOT NULL,
	product_id VARCHAR(30) NOT NULL,
	qty REAL NOT NULL,
	FOREIGN KEY(bill_id) REFERENCES bills(id) ON DELETE CASCADE,
	FOREIGN KEY(product_id) REFERENCES products(id)
);

-- Adding admin account.
INSERT INTO users(uname, password) VALUES('admin', 'admin');