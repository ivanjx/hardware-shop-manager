﻿using HardwareShopManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareShopManager.ViewModels
{
    public class ProductTransactionViewModel
    {
        public ProductTransaction Model { get; private set; }

        public string Name
        {
            get
            {
                return Model.Product.Name;
            }
        }

        public double Quantity
        {
            get
            {
                return Model.Quantity;
            }
        }

        public string UnitName
        {
            get
            {
                return Model.Product.UnitName;
            }
        }


        public ProductTransactionViewModel(ProductTransaction model)
        {
            Model = model;
        }

        public override bool Equals(object obj)
        {
            ProductTransactionViewModel comp;
            if ((comp = obj as ProductTransactionViewModel) != null)
            {
                return Model.Equals(comp.Model);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
