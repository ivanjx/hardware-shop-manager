﻿using HardwareShopManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareShopManager.ViewModels
{
    public class VendorViewModel
    {
        string m_address;

        public Vendor Model { get; private set; }

        public string Name
        {
            get
            {
                return Model.Name;
            }
        }

        public string Phone
        {
            get
            {
                return Model.Phone;
            }
        }

        public string Address
        {
            get
            {
                return m_address;
            }
        }


        public VendorViewModel(Vendor model)
        {
            Model = model;
            m_address = model.Address.Replace(Environment.NewLine, " ");
        }

        public override bool Equals(object obj)
        {
            VendorViewModel comp;
            if ((comp = obj as VendorViewModel) != null)
            {
                return Model.Equals(comp.Model);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
