﻿using HardwareShopManager.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareShopManager.ViewModels
{
    public class BillViewModel
    {
        public Bill Model { get; private set; }

        public string ID
        {
            get
            {
                return Model.ID;
            }
        }

        public string BillType
        {
            get
            {
                if (Model.Type == Bill.BillType.Cash)
                {
                    return "CASH";
                }
                else
                {
                    return "CREDIT";
                }
            }
        }

        public string CustomerName
        {
            get
            {
                return Model.CustomerName;
            }
        }

        public string CustomerPhone
        {
            get
            {
                return Model.CustomerPhone;
            }
        }

        public string CustomerAddress
        {
            get
            {
                return Model.CustomerAddress.Replace(Environment.NewLine, " ");
            }
        }

        public string BillDateStr
        {
            get
            {
                return Model.BillDate.ToString("MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            }
        }


        public BillViewModel(Bill model)
        {
            Model = model;
        }

        public override bool Equals(object obj)
        {
            BillViewModel comp;
            if ((comp = obj as BillViewModel) != null)
            {
                return Model.Equals(comp.Model);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
