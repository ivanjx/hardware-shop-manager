﻿using HardwareShopManager.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareShopManager.ViewModels
{
    public class PurchaseViewModel
    {
        public Purchase Model { get; private set; }

        public string ID
        {
            get
            {
                return Model.ID;
            }
        }

        public string PurchaseDateString
        {
            get
            {
                return Model.PurchaseDate.ToString("MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            }
        }


        public PurchaseViewModel(Purchase model)
        {
            Model = model;
        }

        public override bool Equals(object obj)
        {
            PurchaseViewModel comp;
            if ((comp = obj as PurchaseViewModel) != null)
            {
                return Model.Equals(comp.Model);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
