﻿using HardwareShopManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareShopManager.ViewModels
{
    public class ProductViewModel
    {
        public Product Model { get; private set; }

        public string ID
        {
            get
            {
                return Model.ID;
            }
        }

        public string Name
        {
            get
            {
                return Model.Name;
            }
        }

        public string VendorName
        {
            get
            {
                return Model.VendorName;
            }
        }

        public double Quantity
        {
            get
            {
                return Model.Quantity;
            }
        }

        public string UnitName
        {
            get
            {
                return Model.UnitName;
            }
        }

        public string BuyPriceString
        {
            get
            {
                return Model.BuyPrice.ToString("C");
            }
        }

        public string SellPriceString
        {
            get
            {
                return Model.SellPrice.ToString("C");
            }
        }

        public float VAT
        {
            get
            {
                return Model.VAT;
            }
        }


        public ProductViewModel(Product model)
        {
            Model = model;
        }

        public override bool Equals(object obj)
        {
            ProductViewModel comp;
            if ((comp = obj as ProductViewModel) != null)
            {
                return Model.Equals(comp.Model);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
