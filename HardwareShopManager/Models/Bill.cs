﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareShopManager.Models
{
    public class Bill
    {
        public enum BillType
        {
            Cash,
            Credit
        }

        public string ID { get; set; }

        public string CustomerName { get; set; }

        public string CustomerPhone { get; set; }

        public string CustomerAddress { get; set; }

        public DateTime BillDate { get; set; }

        public BillType Type { get; set; }

        public List<ProductTransaction> SoldProducts { get; private set; }


        public Bill()
        {
            SoldProducts = new List<ProductTransaction>();
        }

        public override bool Equals(object obj)
        {
            Bill comp;
            if ((comp = obj as Bill) != null)
            {
                return ID == comp.ID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
