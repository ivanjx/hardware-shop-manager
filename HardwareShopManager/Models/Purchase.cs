﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareShopManager.Models
{
    public class Purchase
    {
        public string ID { get; set; }

        public DateTime PurchaseDate { get; set; }

        public List<ProductTransaction> PurchasedProducts { get; private set; }


        public Purchase()
        {
            PurchasedProducts = new List<ProductTransaction>();
        }

        public override bool Equals(object obj)
        {
            Purchase comp;
            if ((comp = obj as Purchase) != null)
            {
                return ID == comp.ID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
