﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareShopManager.Models
{
    public class Product
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public string VendorID { get; set; }

        public string VendorName { get; set; }

        public double Quantity { get; set; }

        public string UnitName { get; set; }

        public decimal BuyPrice { get; set; }

        public decimal SellPrice { get; set; }

        public float VAT { get; set; }


        public override bool Equals(object obj)
        {
            Product comp;
            if ((comp = obj as Product) != null)
            {
                return ID == comp.ID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
