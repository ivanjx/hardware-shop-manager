﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareShopManager.Models
{
    public class Vendor
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }


        public override bool Equals(object obj)
        {
            Vendor comp;
            if ((comp = obj as Vendor) != null)
            {
                return comp.ID == ID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
