﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareShopManager.Models
{
    public class ProductTransaction
    {
        public string ProductID { get; set; }

        public double Quantity { get; set; }

        public Product Product { get; set; } // Beware, this can be null!!!


        public override bool Equals(object obj)
        {
            ProductTransaction comp;
            if ((comp = obj as ProductTransaction) != null)
            {
                return ProductID == comp.ProductID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
