﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareShopManager.Commons
{
    public enum DialogResult
    {
        None,
        OK,
        Cancel
    }
}
