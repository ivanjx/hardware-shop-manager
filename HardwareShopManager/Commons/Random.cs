﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareShopManager.Commons
{
    public static class Random
    {
        const string m_alphanumerics = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        static System.Random random = new System.Random();

        public static Task<string> RandomStringAsync(int length)
        {
            return Task.Run(() => RandomString(length));
        }

        public static string RandomString(int length)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; ++i)
            {
                char ch = m_alphanumerics[random.Next(m_alphanumerics.Length)];
                sb.Append(ch);
            }
            return sb.ToString();
        }
    }
}
