﻿using HardwareShopManager.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareShopManager.Commons
{
    public static class Data
    {
        public static async Task<List<Vendor>> LoadVendorsAsync(SQLiteConnection conn)
        {
            List<Vendor> vendors = new List<Vendor>();

            using (SQLiteCommand cmd = new SQLiteCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "SELECT * FROM vendors ORDER BY name ASC";
                DbDataReader reader = await cmd.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    Vendor vendor = new Vendor();
                    vendor.ID = reader.GetString(0);
                    vendor.Name = reader.GetString(1);
                    vendor.Phone = reader.GetString(2);
                    vendor.Address = reader.GetString(3);
                    vendors.Add(vendor);
                }
            }

            return vendors;
        }

        public static async Task<List<Product>> LoadProductsAsync(SQLiteConnection conn)
        {
            List<Product> products = new List<Product>();

            using (SQLiteCommand cmd = new SQLiteCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "SELECT products.id, products.name, products.vendor_id, vendors.name, products.qty, products.unit_name, products.price_buy, products.price_sell, products.vat ";
                cmd.CommandText += "FROM products, vendors ";
                cmd.CommandText += "WHERE products.vendor_id = vendors.id ";
                cmd.CommandText += "ORDER BY products.name ASC";
                DbDataReader reader = await cmd.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    Product product = new Product();
                    product.ID = reader.GetString(0);
                    product.Name = reader.GetString(1);
                    product.VendorID = reader.GetString(2);
                    product.VendorName = reader.GetString(3);
                    product.Quantity = reader.GetDouble(4);
                    product.UnitName = reader.GetString(5);
                    product.BuyPrice = reader.GetDecimal(6);
                    product.SellPrice = reader.GetDecimal(7);
                    product.VAT = reader.GetFloat(8);
                    products.Add(product);
                }
            }

            return products;
        }

        public static async Task<List<Purchase>> LoadPurchasesAsync(SQLiteConnection conn)
        {
            List<Purchase> purchases = new List<Purchase>();
            List<Product> products = await LoadProductsAsync(conn);

            using (SQLiteCommand cmd = new SQLiteCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "SELECT * FROM purchases ORDER BY purchase_date DESC";
                DbDataReader purchaseReader = await cmd.ExecuteReaderAsync();

                while (await purchaseReader.ReadAsync())
                {
                    Purchase purchase = new Purchase();
                    purchase.ID = purchaseReader.GetString(0);
                    purchase.PurchaseDate = purchaseReader.GetDateTime(1);

                    // Populating the purchased products info.
                    using (SQLiteCommand linkCmd = new SQLiteCommand())
                    {
                        linkCmd.Connection = conn;
                        linkCmd.CommandText = "SELECT product_id, qty FROM purchase_product_link WHERE purchase_id = @purchaseId";
                        linkCmd.Parameters.AddWithValue("purchaseId", purchase.ID);
                        DbDataReader linkReader = await linkCmd.ExecuteReaderAsync();

                        while (await linkReader.ReadAsync())
                        {
                            ProductTransaction purchasedProduct = new ProductTransaction();
                            purchasedProduct.ProductID = linkReader.GetString(0);
                            purchasedProduct.Quantity = linkReader.GetDouble(1);

                            // Looking for matching Product instance by the ID.
                            for (int i = 0; i < products.Count; ++i)
                            {
                                if (products[i].ID == purchasedProduct.ProductID)
                                {
                                    purchasedProduct.Product = products[i];
                                }
                            }

                            purchase.PurchasedProducts.Add(purchasedProduct);
                        }
                    }

                    purchases.Add(purchase);
                }
            }

            return purchases;
        }

        public static async Task<List<Bill>> LoadBillsAsync(SQLiteConnection conn)
        {
            List<Bill> bills = new List<Bill>();
            List<Product> products = await LoadProductsAsync(conn);

            using (SQLiteCommand cmd = new SQLiteCommand(conn))
            {
                cmd.CommandText = "SELECT * FROM bills ORDER BY bill_date DESC";
                DbDataReader billsReader = await cmd.ExecuteReaderAsync();

                while (await billsReader.ReadAsync())
                {
                    Bill bill = new Bill();
                    bill.ID = billsReader.GetString(0);

                    string type = billsReader.GetString(1);
                    if (type == "CASH")
                    {
                        bill.Type = Bill.BillType.Cash;
                    }
                    else if (type == "CREDIT")
                    {
                        bill.Type = Bill.BillType.Credit;
                    }
                    else
                    {
                        throw new InvalidDataException("Invalid bill type");
                    }

                    bill.CustomerName = billsReader.GetString(2);
                    bill.CustomerAddress = billsReader.GetString(3);
                    bill.CustomerPhone = billsReader.GetString(4);
                    bill.BillDate = billsReader.GetDateTime(5);

                    // Populating sold products.
                    using (SQLiteCommand linkCmd = new SQLiteCommand(conn))
                    {
                        linkCmd.CommandText = "SELECT product_id, qty FROM bill_product_link WHERE bill_id = @billId";
                        linkCmd.Parameters.AddWithValue("billId", bill.ID);
                        DbDataReader linkReader = await linkCmd.ExecuteReaderAsync();

                        while (await linkReader.ReadAsync())
                        {
                            ProductTransaction pt = new ProductTransaction();
                            pt.ProductID = linkReader.GetString(0);
                            pt.Quantity = linkReader.GetDouble(1);

                            // Looking for matching Product instance by the ID.
                            for (int i = 0; i < products.Count; ++i)
                            {
                                if (products[i].ID == pt.ProductID)
                                {
                                    pt.Product = products[i];
                                }
                            }

                            bill.SoldProducts.Add(pt);
                        }
                    }

                    bills.Add(bill);
                }
            }

            return bills;
        }
    }
}
